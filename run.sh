#!/usr/bin/env bash

set -Eeuo pipefail

printf "Exploding frames...\n"
mkdir -p frames_in frames_out
ffmpeg -i ${1} -vf fps=30 -f image2 frames_in/image-%05d.jpg

printf "Dreaming...\n"
parallel --joblog /tmp/dbdream.log --bar --files --results /tmp/dbdreamlog -j1 python deepdream.py {} frames_out/{/} ::: frames_in/*.jpg > /dev/null

printf "Reassembling frames...\n"
ffmpeg -framerate 30 -i frames_out/image-%05d.jpg output.mp4
